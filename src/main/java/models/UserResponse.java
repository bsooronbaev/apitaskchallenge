package models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserResponse {
    @JsonProperty("isSuccess")
    private boolean isSuccess;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("errorCode")
    private int errorCode;
    @JsonProperty("errorMessage")
    private String errorMessages;
    @JsonProperty("user")
    private User user;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class User {
        @JsonProperty("id")
        private int id;
        @JsonProperty("name")
        private String name;
        @JsonProperty("gender")
        private String gender;
        @JsonProperty("age")
        private int age;
        @JsonProperty("city")
        private String city;
        @JsonProperty("registrationDate")
        private String registrationDate;
    }

}