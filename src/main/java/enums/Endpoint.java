package enums;

public enum Endpoint {

    BASE_URL ("https://hr-challenge.dev.tapyou.com/api/test"),
    FEMALE("/users?gender=female"),
    MALE("/users?gender=male"),
    ID("/user/");

    private String value;

    public String getValue() {
        return value;
    }

    Endpoint(String value) {
        this.value = value;
    }
}
