package methods;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import static io.restassured.RestAssured.given;

public class ApiMethods {
    RequestSpecification spec;
    String baseUrl;

    public ApiMethods(String baseUrl) {
        this.baseUrl = baseUrl;
        this.spec = given()
                .baseUri(baseUrl);
    }
    public Response get (String endPoint){
        return given()
                .spec(this.spec)
                .get(endPoint);
    }
}
