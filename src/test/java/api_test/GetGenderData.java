package api_test;

import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import methods.ApiMethods;
import models.GenderResponse;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


import static enums.Endpoint.*;

public class GetGenderData {
    static ApiMethods apiMethods;
    GenderResponse genderRes;
    static Response response;

    @DataProvider(name = "genderData")
    public Object[][] getGenderData() {
        return new Object[][]{
                {MALE.getValue()},
                {FEMALE.getValue(),}
        };
    }


    @Test(dataProvider = "genderData")
    public void getGenderData(String endpoint) {
        apiMethods = new ApiMethods(BASE_URL.getValue());
        response = apiMethods.get(endpoint);
        ValidatableResponse validatableResponse = response.then();
        validatableResponse.statusCode(200);
        genderRes = response.getBody().as(GenderResponse.class);
        Assert.assertTrue(genderRes.isSuccess());
        Assert.assertEquals(genderRes.getErrorCode(), 0);
        Assert.assertNull(genderRes.getErrorMessages());
        Assert.assertNotNull(genderRes.getIdList());
    }
}
