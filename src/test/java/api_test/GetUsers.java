package api_test;

import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import methods.ApiMethods;
import models.GenderResponse;
import models.UserResponse;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.ArrayList;
import java.util.List;

import static enums.Endpoint.*;


public class GetUsers {


    static ApiMethods apiMethods;
    GenderResponse genderRes;
    static Response response;
    static List<Integer> idList = new ArrayList<>();

    static UserResponse user;


    @DataProvider(name = "genderData")
    public Object[][] getGenderData() {
        return new Object[][]{
                {MALE.getValue(), "male"},
                {FEMALE.getValue(), "female"}
        };
    }


    @Test(dataProvider = "genderData")
    public void getUsersData(String endpoint, String gender) {
        apiMethods = new ApiMethods(BASE_URL.getValue());
        response = apiMethods.get(endpoint);
        genderRes = response.getBody().as(GenderResponse.class);
        idList = new ArrayList<>(List.of(genderRes.getIdList()));
        getUsersById(gender);

    }

    public static void getUsersById(String gender) {
        SoftAssert softAssert = new SoftAssert();
        ValidatableResponse validatableResponse = response.then();
        for (int id : idList) {
            response = apiMethods.get(ID.getValue() + id);
            validatableResponse.statusCode(200);
            user = response.getBody().as(UserResponse.class);
            softAssert.assertEquals(user.getUser().getId(), id, "Id = ");
            softAssert.assertNotNull(user.getUser().getName(), "name (Id " + id + ") = ");
            softAssert.assertEquals(user.getUser().getGender(), gender, "Gender (Id " + id + ") = ");
            softAssert.assertTrue(user.getUser().getAge() < 130, "Age (Id " + id + ") Актуальный результат("+ user.getUser().getAge()+" лет)" + " (Должно быть меньше 130 лет) = ");
            softAssert.assertNotNull(user.getUser().getCity(), "City (Id " + id + ") = ");
            softAssert.assertNotNull(user.getUser().getRegistrationDate(), "RegistrationDate (Id " + id + ") = ");


        }
        softAssert.assertAll();

    }

}
